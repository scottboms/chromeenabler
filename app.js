// Add some chrome to this prototype
var chrome = new Browser(true, true, "facebook", "#e9eaed", "#fff", "about/mobile", 944, 2400);

content = new View({
  x: 0,
  y: 0,
  height: 2400,
  width: 944,
  style: {
    "background-color": "transparent",
    "background-image": "url(images/gradient.png)",
    "background-position": "50% 0",
    "background-repeat": "repeat-y"
  },
  superView: contentWrapper
});