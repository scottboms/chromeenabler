# chromeEnabler for Framer 2.0

Adds a customizable browser wrapper for Framer web projects.

## Usage

1. Add the following line to the <head> section of your project's index.html file after the "framer.js" SCRIPT tag and copy the file to your project folder: `<script src="chromeEnabler.js"></script>`
2. In your app.js file, create a new Browser object and set some defaults like so: `var chrome = new Browser(true, true, "facebook", "#e9eaed", "#fff", "facebook.com/about/newsfeed", 1140, 2400);`

## Parameters
  
Parameters must be set in the defined order (chromeEnabled, blueBarEnabled, fbOrInstagram, bkgdWash, bkgdContent, urlString, wrapperWidth, contentHeight):

1. Enable the browser chrome ( true | false )
2. Enable the UI chrome ( true | false )
3. Choose which UI chrome ( "facebook" | "instagram" )
4. Choose an overall page background wash color ( "hex value" )
5. Choose a content wrapper background wash color ( "hex value" | "" )
6. Set a URL string including the domain ( "facebook.com/about/mobile" | "instagram.com/about" )
7. Set the WIDTH of the content wrapper
8. Set the HEIGHT of the content

# Ideas? Help?

Go here: https://bitbucket.org/scottboms/chomeenabler/issues
