// Add some chrome to this prototype
var chrome = new Browser(true, true, "instagram", "#edeeef", "", "instagram.com/about", 1140, 2400);

content = new View({
  x: (1140 - 944) / 2,
  y: 0,
  height: 2400,
  width: 944,
  style: {
    "background-color": "#fff",
    "background-image": "url(../../images/gradient.png)",
    "background-position": "50% 0",
    "background-repeat": "repeat-y"
  },
  superView: contentWrapper
});